import gradio as gr
from diffusers import DiffusionPipeline
import torch

# Initialize the Stable Diffusion pipeline
pipe = DiffusionPipeline.from_pretrained("stabilityai/stable-diffusion-xl-base-1.0", torch_dtype=torch.float16, use_safetensors=True, variant="fp16")

def generate_image(prompt, negative_prompt, num_inference_steps):
    # Generate the image
    image = pipe(prompt=prompt, negative_prompt=negative_prompt, num_inference_steps=num_inference_steps).images[0]
    return image

# Create the Gradio interface
iface = gr.Interface(
    fn=generate_image,
    inputs=[
        gr.Textbox(label="Prompt", placeholder="Enter your image description here"),
        gr.Textbox(label="Negative Prompt", placeholder="Enter what you don't want in the image"),
        gr.Slider(minimum=1, maximum=100, step=1, label="Number of Inference Steps", value=50)
    ],
    outputs=gr.Image(type="pil", label="Generated Image"),
    title="Stable Diffusion Image Generator",
    description="Generate images using Stability AI's Stable Diffusion model"
)

# Launch the interface
iface.launch()
