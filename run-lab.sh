#!/bin/bash

set -e -x

cd $(dirname $(readlink -f "$0"))

source venv/bin/activate

cd work/

# Limit to 14 Gigabytes
# ulimit -v $((14 * 1024 * 1024))

jupyter lab --no-browser
