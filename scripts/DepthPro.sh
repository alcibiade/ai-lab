#!/bin/bash

huggingface-cli download --local-dir checkpoints apple/DepthPro

git clone https://github.com/apple/ml-depth-pro.git
pip install ./ml-depth-pro/

depth-pro-run -i ./data/example.jpg

